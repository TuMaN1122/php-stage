<?php

$imagePath = __DIR__ . './images/blank.png';
$fontPath = __DIR__.'./9605.ttf';



function t1()
{
    global $imagePath, $fontPath;
    
    $image_1 = imagecreatefrompng($imagePath);
    $color = imagecolorallocate($image_1, 255, 0 , 0);
    $text = date("Y-m-d");
    imagettftext($image_1, 40, 0, 100, 100, $color, $fontPath, $text);
    imagepng($image_1, __DIR__.'/task_1.png');
    imagedestroy($image_1);
}

function t2()
{
    global $imagePath, $fontPath;

    $image_2 = imagecreatefrompng($imagePath);
    $qrCode = imagecreatefrompng(__DIR__ .'/images/flash.png');
    imagecopy($image_2, $qrCode, 500, 0, 0, 0, 256, 256);
    imagepng($image_2, __DIR__.'/task_2.png');
    imagedestroy($qrCode);
    imagedestroy($image_2);
}

function t3()
{
    global $imagePath, $fontPath;

    $image_3 = imagecreatefrompng($imagePath);
    $flashImg = imagecreatefrompng(__DIR__ .'/images/flash.png');
    $spiderImg = imagecreatefrompng(__DIR__ .'/images/spider.png');
    $thorImg = imagecreatefrompng(__DIR__ .'/images/thor.png');
    imagecopy($image_3, $flashImg, 0, 0, 0, 0, 256, 256);
    imagecopy($image_3, $spiderImg, 256, 0, 0, 0, 256, 256);
    imagecopy($image_3, $thorImg, 512, 0, 0, 0, 256, 256);
    imagepng($image_3, __DIR__.'/task_3.png');
    imagedestroy($flashImg);
    imagedestroy($thorImg);
    imagedestroy($spiderImg);
    imagedestroy($image_3);
}

function  t4()
{
    global $imagePath, $fontPath;
    
    $image_4 = imagecreatefrompng($imagePath);
    $color = imagecolorallocate($image_4, 0, 0 , 0);
    $text = 'hello';
    imagettftext($image_4, 36, 45, 50, 150, $color, $fontPath, $text);
    $thorImg = imagecreatefrompng(__DIR__ .'/images/thor.png');
    imagecopy($image_4, $thorImg, 200, 0, 0, 0, 256, 256);
    imagepng($image_4, __DIR__.'/task_4.png');
    imagedestroy($thorImg);
    imagedestroy($image_4);
}

function t5()
{
    $newImg = imagecreatetruecolor(300, 300);
    $flashImg = imagecreatefrompng(__DIR__ .'/images/flash.png');
    imagecopy($newImg, $flashImg, 22, 22, 0, 0, 256, 256);
    imagepng($newImg, __DIR__.'/task_5.png');
    imagedestroy($newImg);
    imagedestroy($flashImg);
}

function t6()
{
    global $fontPath;

    $im = imagecreatetruecolor(512, 256);
    $white = imagecolorallocate($im, 255, 255, 255);
    $colorText = imagecolorallocate($im, 0, 0, 0);
    imagefilledrectangle($im, 0, 0, 512, 256, $white);
    $text = 'hello jpeg';
    imagettftext($im, 50, 0, 100, 100, $colorText, $fontPath, $text);
    imagejpeg($im, __DIR__.'/task_6.jpg');
    imagedestroy($im);

}
