<?php

function t1()
{
    return time();
}

function t2()
{
    return date('D');
}

function t3($year)
{
    $leap = date('L', mktime(0, 0, 0, 1, 1, $year));
    return $leap;
}

function  t4()
{
    return date('m');
}

function t5()
{
    return date('Y-m-d H:i');
}

function t6()
{
    // return $time = (new DateTime('today midnight')) -> getTimestamp();
    // $beginOfDay = strtotime("today", time());
    $beginOfDay = mktime(0, 0, 0, date('n'), date('j'), date('Y'));
    return $beginOfDay;
}

function t7($m)
{
    $beginOfDay =  mktime(0, 0, 0, $m, 1, date('Y'));
    return $beginOfDay;
}

function t8($t)
{
    $givenDate = date('l', $t);
    if ($givenDate == 'Saturday' || $givenDate == 'Sunday') {
        return 1;
    } else {
        return 0;
    }
}

function t9($s)
{
    $a = time();
    $b = strtotime($s);
    return floor(($a - $b) / (60 * 60 * 24));
}

function t10()
{
    $startDate  =  mktime(0, 0, 0, 1, 1, date('Y'));
    $currentDate = time();
    return $currentDate - $startDate;
}
