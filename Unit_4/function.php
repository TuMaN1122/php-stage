<?php

function t1()
{
    global $a, $b, $out1;
    $out1 = '';
    for ($i = $a; $i <= $b; $i++) {
        $out1 .= $i . ' ';
    }
    echo $out1;
}

function t2()
{
    global $d, $e, $out2;
    $out2 = '';
    for ($i = $d; $i <= $e; $i = $i + 2) {
        $out2 .= $i . ' ';
    }

    echo $out2;
}

function t3()
{
    global $f, $g, $out3;
    $out3 = '';
    for ($i = $f; $i >= $g; $i = $i - 2) {
        $out3 .= $i . ' ';
    }
    echo $out3;
}

function t4()
{
    global $num1, $num2, $out4;
    $minNum = $num1;
    $maxNum = $num2;
    if ($num1 > $num2) {
        $minNum = $num2;
        $maxNum = $num1;
    }
    $out4 = '';
    for ($i = $minNum; $i <= $maxNum; $i++) {
        $out4 .= $i . ' ';
    }
    echo $out4;
}

function t5()
{
    global $out5;
    // $num = 0;
    for ($i = 5; $i >= 0; $i--) {
        $out5 .= $i . ' ' . (5 - $i) . ' ';
        // $num = $num + 1;
    }
    echo $out5;
}

function t6()
{
    global $out6, $k;
    $out6 = '';
    for ($i = 1; $i <= $k; $i++) {
        $out6 .= $i . ' * ';
    }
    echo $out6;
}

function t7()
{
    global $out7, $m;
    $out7 = '';
    for ($i = 1; $i <= $m; $i++) {
        if ($i % 2 != 0) {
            $out7 .= $i . ' * ';
        } else {
            $out7 .= $i .' ** ';
        }
    }
    echo $out7;
}

function t8()
{
    global $out8, $n;
    $out8 = '';
    for ($i = 1; $i <= $n; $i++) {
        if ($i % 2 != 0) {
            $out8 .= $i . ' ';
        } else {
            $out8 .= 0 . ' ';
        }
    }
    echo $out8;
}

function t9()
{
    global $out9;
    $out9 = '';
    for ($i = 0; $i < 3; $i++) {
        $out9 .= '***<br>';
    }
    echo $out9;
}

function t10()
{
    global $out10, $t;
    $out10 = '';
    for ($i = $t; $i <= 2020; $i++) {
        if ($i % 2 == 0) {
            $out10 .= $i . ' ';
        }
    }
    echo $out10;
}
