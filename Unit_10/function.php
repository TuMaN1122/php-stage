<?php

function t1($path)
{
    if (file_exists($path)) {
        return 1;
    } else {
        return 0;
    }
}

function t2($path)
{
    if (file_exists($path)) {
        return filesize($path);
    } else {
        return 0;
    }
}

function t3($path)
{
    if (is_dir($path)) {
        return 'dir';
    }
    if (is_file($path)) {
        return 'file';
    }
}

function  t4($path)
{
    $arr = [];
    $infoFile = pathinfo($path);
    array_push($arr, $infoFile['filename'], $infoFile['extension']);
    print_r($arr);
}

function t5($path)
{
    $handle = fopen($path, "r");
    $contents = fread($handle, filesize($path));
    fclose($handle);
    return $contents;
}

function t6($path, $str)
{
    $handle = fopen($path, 'w+');
    fwrite($handle, $str);
    fclose($handle);

    $handle = fopen($path, "r");
    $contents = fread($handle, filesize($path));
    fclose($handle);
    print_r($contents);
}

function t7($path, $str)
{
    $handle = fopen($path, 'a');
    fwrite($handle, $str);
    fclose($handle);

    $handle = fopen($path, "r");
    $contents = fread($handle, filesize($path));
    fclose($handle);
    print_r($contents);
}

function t8($path)
{
    $handle = opendir($path);
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                echo filesize($path .$entry) .'<br>';
            }
        }
    closedir($handle);
}

function t9($path)
{
    $res = [];
    $handle = opendir($path);
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $newarr = explode(" ", $entry);
            $txt = explode(".", $entry);
            foreach ($newarr as $item) {

                $temp = [];
                $item;

                for ($i = 0; $i < count($txt); $i += 2) {
                    $temp[] = $txt[$i];
                }
                for ($i = 1; $i < count($txt); $i += 2) {
                    $temp[] = $txt[$i];
                }
                $temp[] = filesize($path . $item);
                $res[] = $temp;
            }
        }
    }
    closedir($handle);
    print_r($res);
}

function t10($path)
{
    if (file_exists($path)) {
        return 0;
    } else {
        $fp = fopen($path, 'w');
        fwrite($fp, 42);
        fclose($fp);
        return 1;
    }
}
