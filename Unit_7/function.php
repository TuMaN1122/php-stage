<?php

function t1()
{
    if (isset($_GET['inp-1'])){
        return trim($_GET['inp-1']);
    }
    return false;
}

function t2()
{
    // используем функцию https://www.php.net/manual/en/function.max.php
    return max($_GET['inp-2-1'], $_GET['inp-2-2']);
}

function t3()
{
    $pass = $_GET['inp-3'];
    if (isset($pass) AND trim($pass) !== '' AND strlen(trim($pass)) > 5 ){
        return 1;
    } else{
        return 0;
    }
}

function t4()
{

    if ((date('Y') - $_GET['inp-4']) >= 18){
        return 1;
    } else{
        return 0;
    }
}

function t5()
{

    if(isset($_GET['i-5'])){
        return 1;
    } else{
        return 0;
    }
}

function t6()
{
    // тут ваш код
    // var_dump($_GET['radio-1']);
    if(isset($_GET['radio-1'])){
        return $_GET['radio-1'];
    }
    // return $_GET['radio-1'];
}

function t7()
{
    if(isset($_POST['radio-2'])){
        return $_POST['radio-2'];
    }
    // return $_POST['radio-2'];
}

function t8()
{
    if(isset($_POST['i-8'])){
        return 1;
    } else{
        return 0;
    }
}

function t9(){
    // str_contains
    // var_dump($_POST['i-9']);
    if(str_contains($_POST['i-9'], '@')){
        return 1;
    } else{
        return 0;
    }

}

function t10() {
    // return $_POST['i-10'];
    if (isset($_POST['i-10'])){
        return $_POST['i-10'];
    }
    return false;
}