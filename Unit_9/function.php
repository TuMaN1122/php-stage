<?php

function t1($num, $arr)
{
    if (in_array($num, $arr)) {
        return 'yes';
    } else {
        return 'no';
    }
}

function t2($num, $arr)
{
    $a = 0;
    for ($i = 0; $i < count($arr); $i++) {
        if ($arr[$i] === $num) {
            $a = $a + 1;
        }
    }
    return $a;
}

function t3($num, $arr)
{
    $a = 0;
    for ($i = 0; $i < count($arr); $i++) {
        if ($arr[$i] < $num) {
            $a = $a + 1;
        }
    }
    return $a;
}

function  t4($num, $arr)
{
    for ($i = 0; $i < count($arr); $i++) {
        if ($arr[$i] > $num) {
            $a[] = $arr[$i];
        }
    }
    return $a;
}

function t5($num, $arr)
{
    $a = 0;
    if ($num == 'even') {
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i] % 2 === 0) {
                $a++;
            }
        }
        return 'even = ' . $a;
    }
    if ($num == 'odd') {
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i] % 2 !== 0) {
                $a++;
            }
        }
        return 'odd = ' . $a;
    }
}

function t6($ar)
{
    $ar6 = array_reverse($ar);
    return $ar6;
}

function t7($arr)
{
    sort($arr);
    return $arr;
}

function t8($arr)
{
    array_pop($arr);
    return $arr;
}
function t9($arr)
{
    array_shift($arr);
    return $arr;
}

function t10($num, $arr)
{
    for ($i = 0; $i < count($arr); $i++) {
        if ($num == $arr[$i]) {
            return $i;
        }
    }
    return 0;
}
