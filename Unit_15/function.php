<?php

function t1()
{
    $zip = new ZipArchive(); //Создаём объект для работы с ZIP-архивами
    $filename = "./1.zip"; // Название архива
    $zip->open($filename, ZipArchive::CREATE); //Открываем (создаём) архив 1.zip
    $zip->addFile("t1.txt"); //Добавляем в архив файл t1.txt
    $zip->close(); //Завершаем работу с архивом
}

function t2()
{
    echo 't1.txt = ' . filesize("t1.txt") . ' байтов' . '<br>';
    $zip = new ZipArchive();
    $filename = "./2.zip";
    $zip->open($filename, ZipArchive::CREATE);
    $zip->addFile("t1.txt");
    $zip->close();
    echo $filename . ' = ' . filesize("2.zip") . ' байтов' . '<br>';
}

function t3()
{
    $zip = new ZipArchive();
    $filename = "./3.zip";
    $zip->open($filename, ZipArchive::CREATE);
    $zip->addFile("t1.txt");
    $zip->addFile("images/flash.png");
    $zip->close();
}

function  t4()
{
    $zip = new ZipArchive();
    $filename = "./4.zip";
    $zip->open($filename, ZipArchive::CREATE);
    $zip->addEmptyDir('one');
    $zip->addFile('t1.txt', 'one/t1.txt');
    $zip->close();
}

function t5($arc, $folder)
{
    $zip = new ZipArchive;
    $zip->open($arc);
    $zip->extractTo($folder);
    $zip->close();
}
