<?php

//Создать функцию t1, которая выводит содержимое файла book1.json на страницу. Формат вывода - элементы строки через пробел, строки - через br.
function t1()
{
    $res = file_get_contents('./one/book1.json');
    $data = json_decode($res, true);

    foreach($data as $x => $val) {
        echo "$x $val<br>";
    }
}




//Создать функцию t2, которая принимает параметр - путь к файлу json и возвращает его содержимое преобразованное в массив.
function t2($path)
{
    $res = file_get_contents($path);
    $arr = json_decode($res, true);
    return $arr;
}





function t3($path)
{
    $res = file_get_contents($path);
    $arr = json_decode($res, true);
    $str = '';

    foreach($arr as $x => $val){
        $str .= $arr[$x]['name'] .' ';
    }
    // for($i = 0; $i < count($arr); $i++){
    //     $str .= $arr[$i]['name'] .' ';
    // }
    return $str;
}

function  t4($arr, $path)
{
file_put_contents($path, json_encode($arr));
}

function t5($path)
{
    $res = file_get_contents('./one/book1.json');
    $arr = json_decode($res, true);
    return count($arr);
}
