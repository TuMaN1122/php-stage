<?php

function t2()
{
    $query = 'SELECT * FROM cars';
    return select($query);
}

function t3()
{
    // $query = 'SELECT * FROM cars';
    // $arr_t3 = select($query);
    // $arr_string = [];
    // for ($i = 0; $i < count($arr_t3); $i++) {
    //     if ($arr_t3[$i]['cost'] > 100000) {
    //         $arr_string[] = $arr_t3[$i];
    //     }
    // }
    // return $arr_string;
    $query = 'SELECT * from cars where  cost >100000';
    return select($query);
}

function  t4()
{
    $query = 'SELECT * from cars';
    $arr_t4 = select($query);
    $arr_name = [];
    for ($i = 0; $i < count($arr_t4); $i++) {
        $arr_name[] = $arr_t4[$i]['cars'];
    }
    return $arr_name;
}

function t5()
{
    $inseret_t5 = execQuery('INSERT INTO cars (cars, image, cost) VALUES ("Cadillac Escalade Platinum", "http://elite.cars.ua/img/upload/cache/zc-1_iar-1_h-357_w-570_5ecd1aa8a55af0_68546755.jpg", 47777);');
    return $inseret_t5;
}

function t6()
{
    $update_t6 = execQuery('UPDATE cars SET cars = "Cadillac Escalade", cost = 47500 WHERE cars = "Cadillac Escalade Platinum"');
    return $update_t6;
}

function t7()
{

    $query = 'UPDATE cars SET cost=cost-2800 WHERE cost>100000 AND cost<200000';
    return execQuery($query);
}

function t8()
{
    $query = select('SELECT * from cars');
    $sum = 0;
    for ($i = 0; $i < count($query); $i++) {
        $sum += $query[$i]['cost'];
    }
    return $sum;
}

function t9()
{
    $query = select('SELECT * from cars');
    $arr_t9 = [];
    for ($i = 0; $i < count($query); $i++) {
        $arr_t9[$query[$i]['cars']] = $query[$i]['cost'];
    }
    return $arr_t9;
}

function t10()
{
    $delete_t10 = execQuery('DELETE FROM cars WHERE cars = "Cadillac Escalade"');
    return $delete_t10;
}
