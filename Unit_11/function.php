<?php

function t1()
{
    if (($file = fopen('./one/book1.csv', 'r')) !== false) {
        while (($data = fgetcsv($file, 1000, ";")) !== false) {
            $out = '';
            for ($i = 0; $i < count($data); $i++) {
                $out .=$data[$i]." ";
            }
            echo $out .'<br>';
        }
        fclose($file);
    }
}

function t2($path)
{
    $res = [];
    if (($file = fopen($path, 'r')) !== false) {
        while (($data = fgetcsv($file, 1000, ";")) !== false) {
            $res[] = $data;
        }
        fclose($file);
    }
    return $res;
}

function t3($path)
{
    $res = [];
    $out = '';
    if (($file = fopen($path, 'r')) !== false) {
        while (($data = fgetcsv($file, 1000, ';')) !== false) {
            $res[] = $data;
        }
    }
    for ($i = 1; $i < count($res); $i++) {
        $out .=$res[$i][2] . ' ';
    }
    fclose($file);
    return $out;
}

function  t4($arr, $path)
{
    $file = fopen($path, 'w');
    foreach($arr as $line){
        fputcsv($file, $line, ';');
    }
    
    fclose($file);
}

function t5($path)
{
    $num = 0;
    if (($file = fopen($path, 'r')) !== false) {
        while (($data = fgetcsv($file, 1000, ";")) !== false) {
            for ($i = 0; $i < count($data); $i++) {
                $num++;
            }
        }
        fclose($file);
    }
    return $num;
}
