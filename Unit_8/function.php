<?php

function t1()
{
    $a = ['1', 2, true, false, 'hello'];
    return $a;
}

function t2()
{
    $a =  [true, false];
    return $a;
}

function t3()
{
    for ($i = 1; $i <= 100; $i++) {
        $a[] = $i;
    }
    return $a;
}

function t4()
{
    $a[10] = 5;
    $a[15] = 11;
    return $a;
}

function t5($ar)
{
    return $ar[5];
}

function t6($ar)
{
    $a = $ar[2];
    $ar[2] = $ar[4];
    $ar[4] = $a;
    return $ar;
}

function t7($arr)
{
    $a = $arr[0];
    $arr[0] = $arr[count($arr) - 1];
    $arr[count($arr) - 1] = $a;
    return $arr;
}

function t8($arr)
{
    for ($i = 0; $i < count($arr); $i++) {
        if ($arr[$i] < 0) {
            return $arr[$i];
        }
    }
}
function t9($arr)
{
    $a = 0;
    for ($i = 0; $i < count($arr); $i++) {
        if ($arr[$i] < 0) {
            $a = $arr[$i];
        }
    }
    return $a;
}

function t10($num, $arr)
{
    for ($i = 0; $i < count($arr); $i++) {
        if ($arr[$i] === $num) {
            return 1;
        }
    }
    return 0;
}
