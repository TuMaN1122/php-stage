<?php

function t1()
{
    global $a, $b, $out1;
    $out1 = '';
    $i = $a;
    while ($i <= $b) {
        $out1 .= $i . ' ';
        $i = $i + 1;
    }
    echo $out1;
}

function t2()
{
    global $d, $e, $out2;

    $out2 = '';
    $i = $d;
    while ($i <= $e) {
        $out2 .= $i . ' ';
        $i = $i + 2;
    }
    echo $out2;
}

function t3()
{
    global $f, $g, $out3;
    $out3 = '';
    $i = $f;
    while ($i >= $g) {
        $out3 .= $i . ' ';
        $i = $i - 2;
    }
    echo $out3;
}

function t4()
{
    global $num1, $num2, $out4;
    $out4 = '';
    $minNum = $num1;
    $maxNum = $num2;
    if ($num1 > $num2) {
        $minNum = $num2;
        $maxNum = $num1;
    }
    while ($minNum <= $maxNum) {
        $out4 .= $minNum . ' ';
        $minNum++;
    }
    echo $out4;
}

function t5()
{
    global $out5;
    $out5 = '';
    $i = 5;
    while ($i >= 0) {
        $out5 .= $i . ' ' . (5 - $i) . ' ';
        $i--;
    }
    echo $out5;
}

function t6()
{
    global $out6, $k;
    $out6 = '';
    $i = 1;
    while ($i <= $k) {
        $out6 .= $i . ' * ';
        $i++;
    }

    echo $out6;
}

function t7()
{
    global $out7, $m;
    $out7 = '';
    $i = 1;
    while ($i <= $m) {
        if ($i % 2 != 0) {
            $out7 .= $i . ' * ';
        } else {
            $out7 .= $i . ' ** ';
        }
        $i++;
    }
    echo $out7;
}

function t8()
{
    global $out8, $n;
    $out8 = '';
    $i = 1;
    while ($i <= $n) {
        if ($i % 2 == 0) {
            $out8 .= 0 . ' ';
        } else {
            $out8 .= $i . ' ';
        }
        $i++;
    }
    echo $out8;
}

function t9()
{
    global $out9;
    $out9 = '';
    $i = 0;
    while ($i < 3) {
        $out9 .= '***<br>';
        $i++;
    }

    echo $out9;
}

function t10()
{
    global $out10, $t;
    $out10 = '';
    while ($t <= 2020) {
        if ($t % 2 == 0) {
            $out10 .= $t . ' ';
        }
        $t = $t + 1;
    }

    echo $out10;
}

function t11()
{
    global $out11, $num1, $num2;
    $out11 = '';
    $sum = 0;
    $i = $num1;
    while ($i <= $num2) {
        $sum = $sum + $i;
        $i++;
    }
    $out11 = $sum;

    echo $out11;
}

function t12()
{
    global $out12, $num4, $num5;
    $out12 = '';
    $multiplic = 1;
    $i = $num4;
    while ($i <= $num5) {
        $multiplic = $multiplic * $i;
        $i++;
    }
    $out12 = $multiplic;
    echo $out12;
}
